# teeter-totter

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Screenshots

![](screenshots/01.png)
![](screenshots/02.png)
![](screenshots/03.png)


### References
* https://cli.vuejs.org/guide/cli-service.html
* https://cli.vuejs.org/config/
* https://vuex.vuejs.org/
* https://github.com/kelektiv/node-uuid
* https://github.com/bedumur/Insider-vue/
