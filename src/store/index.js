import Vue from 'vue';
import Vuex from 'vuex';
import {
  TOGGLE_START,
  ADD_RIGHT_SIDE_BLOCK,
  ADD_LEFT_SIDE_BLOCK,
  RESET_STATE,
  MAX_BENDING,
  MIN_BENDING,
  MAX_SIDES_DIFFERENCE,
  FALLING_BLOCKS_COUNT,
  FINISH_FALLING,
  NEW_GAME,
  INITIALIZE_FALLING_BLOCKS,
  ADD_FALLING_BLOCK,
  MOVE_RIGHT,
  MOVE_LEFT,
} from '../constants';
import { generateRandomBlock } from '../helpers';

Vue.use(Vuex);

function getBlockPower(array) {
  return array.reduce((acc, item) => acc += item.weight * item.offset, 0);
}

export default new Vuex.Store({
  state: {
    isPaused: true,
    leftSideBlocks: [],
    rightSideBlocks: [],
    movingObjects: [],
  },
  getters: {
    leftSum(state) {
      return getBlockPower(state.leftSideBlocks);
    },
    rightSum(state) {
      return getBlockPower(state.rightSideBlocks);
    },
    swingBending(state, getters) {
      const { leftSum, rightSum } = getters;

      if (!leftSum) return 0;
      if (leftSum === rightSum) return 0;
      return leftSum > rightSum
        ? (leftSum - rightSum) / leftSum * -100
        : (rightSum - leftSum) / rightSum * 100;
    },
    gameOverStatus(state, getters) {
      const { leftSum, rightSum, swingBending } = getters;

      return swingBending > MAX_BENDING
        || swingBending < MIN_BENDING
        || Math.abs(leftSum - rightSum) > MAX_SIDES_DIFFERENCE;
    },
  },
  mutations: {
    [TOGGLE_START](state) {
      state.isPaused = !state.isPaused;
    },
    [ADD_RIGHT_SIDE_BLOCK](state) {
      const randomBlock = generateRandomBlock();
      state.rightSideBlocks.push(randomBlock);
    },
    [ADD_LEFT_SIDE_BLOCK](state) {
      const block = state.movingObjects.shift();
      state.leftSideBlocks.push(block);
    },
    [INITIALIZE_FALLING_BLOCKS](state) {
      state.movingObjects = [];
      for (let i = 0; i < FALLING_BLOCKS_COUNT; i++) {
        const randomBlock = generateRandomBlock();
        state.movingObjects.push(randomBlock);
      }
    },
    [ADD_FALLING_BLOCK](state) {
      const randomBlock = generateRandomBlock();
      state.movingObjects.push(randomBlock);
    },
    [MOVE_RIGHT](state) {
      if (state.isPaused || state.movingObjects[0].offset - 1 <= 0) return;
      state.movingObjects[0].offset -= 1;
    },
    [MOVE_LEFT](state) {
      if (state.isPaused || state.movingObjects[0].offset + 1 > 5) return;
      state.movingObjects[0].offset += 1;
    },
    [RESET_STATE](state) {
      state.isPaused = true;
      state.leftSideBlocks = [];
      state.rightSideBlocks = [];
      state.movingObjects = [];
    },
  },
  actions: {
    [FINISH_FALLING]({
      commit, state, getters, dispatch,
    }) {
      commit(ADD_LEFT_SIDE_BLOCK);
      commit(ADD_FALLING_BLOCK);
      commit(ADD_RIGHT_SIDE_BLOCK);

      if (getters.gameOverStatus) {
        setTimeout(() => {
          document.querySelector('#gameOver').style.display = 'inline-block';
          setTimeout(() => {
            commit(TOGGLE_START);
          }, 100);
        }, 500);
      }
    },
    [NEW_GAME]({ commit }) {
      document.querySelector('#gameOver').style.display = 'none';
      commit(RESET_STATE);
      commit(ADD_RIGHT_SIDE_BLOCK);
      commit(INITIALIZE_FALLING_BLOCKS);
      setTimeout(() => {
        commit(TOGGLE_START);
      }, 100);
    },
  },
  strict: process.env.NODE_ENV !== 'production',
});
